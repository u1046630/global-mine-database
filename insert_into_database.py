#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 13:16:05 2020

@author: liam
"""

import geopandas as gpd
import glob, progressbar, os
from sqlalchemy import create_engine
from shapely.geometry import Polygon, MultiPolygon

res = 1/3600 # resolution of GDEM in degrees (1 arc second ~ 30m)
input_pattern = 'data/pit_cache/*.geojson'
database = 'postgis_db'
table = 'minepit_table2'

def remove_holes(geom):
    if geom.type == 'Polygon':
        return Polygon(list(geom.exterior.coords))
    elif geom.type == 'MultiPolygon':
        return MultiPolygon([remove_holes(poly) for poly in geom.geoms])
    else:
        print('[!] unsupported shape type "{geom.type}". skipping.')
        return geom

db_connection_url = f'postgres://postgis_test:postgispassword@localhost:5432/{database}'
engine = create_engine(db_connection_url)

first = True
for fname in progressbar.progressbar(list(glob.glob(input_pattern))):
    
    if os.stat(fname).st_size == 0:
        continue
    gdf = gpd.read_file(fname, driver='GeoJSON')
    
    # buffer by half cell width to join multipolygons into polygons
    new_geoms = []
    for geom in gdf.geometry:
        new_geom = remove_holes(geom)
        if new_geom.type == 'Polygon':
            new_geom = MultiPolygon([new_geom])
        new_geoms.append(new_geom)
    gdf.geometry = new_geoms
    
    if first:
        gdf.to_postgis(name=table, con=engine, if_exists='replace')
        first = False
    else:
        gdf.to_postgis(name=table, con=engine, if_exists='append')