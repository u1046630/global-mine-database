#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 21:25:23 2020

@author: liam

About: 
input_fname is a shapefile of mining areas
aster30m_dir contains raster files with 30m resolution of elevation
using elevation data, find mining pits inside the mining areas

Approach:
Pretend there is flood water slowing rising outside of the mining area.
Usually, a cell at elevation 100m will get wet when the flood waters 
reach 100m. But the inside a pit, a cell at elevation 30m will get wet 
when the flood waters reach the top of the pit at 120m. So slowly raise
the flood waters, and record the flood water height at which each cell
gets wet. Then look at the difference between this and the elevation 
profile, it will reveal the depth profile of zero or more pit shapes.
"""

import math, pyproj, progressbar, os, random
import numpy as np, geopandas as gpd
import matplotlib.pyplot as plt

import rasterio
from rasterio.merge import merge
from rasterio.mask import mask
from scipy.ndimage.measurements import label
from shapely.geometry import Point, mapping, box
from shapely.ops import cascaded_union
#import matplotlib.pyplot as plt
    
###############
### globals ###
###############

aster30m_dir = '/media/liam/drive2/gdem/data/'
temp_fname = '/tmp/temp.tif'
input_fname = 'data/mining_areas_combined/combined.shp'
output_dir = 'data/pit_cache/' # one shapefile per input polygon
minimum_volume = 0 # GL
minimum_depth = 0 # m
minimum_area = 10 * 10000 # m2 (10 ha)
top_edge_margin = 1 # m (define the pit as only being filled 2m from the top, so shape is nicer)
res = 1.0/3600 # GDEM resolution in degrees (~30m)

# so analysis doesn't take forever, set maximum number of local minima to investigate
# (if too many, smooth elevation profile a little)
max_num_minima = 1000
geod = pyproj.Geod(ellps='WGS84')

#####################
### utility funcs ###
#####################

def latlon_scaling(lat, lon):
    _,_,m_per_lat = geod.inv(lon, lat-0.5, lon, lat+0.5) # m per degree of lat
    _,_,m_per_lon = geod.inv(lon-0.5, lat, lon+0.5, lat) # m per degree of long
    return m_per_lat, m_per_lon
    
# find aster30m tif file that covers this lat/long
def aster30m_fname(x, y):
    c1 = 'N' if y >= 0 else 'S'
    c2 = 'E' if x >= 0 else 'W'
    x = abs(math.floor(x))
    y = abs(math.ceil(y))
    return aster30m_dir + '{}{:02d}{}{:03d}.tif'.format(c1, y, c2, x)
    
# polygon: shapely.geometry.polygon.Polygon (mine area)
# out: numpy.ndarray of values from rasters that cover (elevation)
def load_raster_file(polygon):
    minX,minY,maxX,maxY = polygon.bounds
    minX,maxX = math.floor(minX), math.floor(maxX)
    minY,maxY = math.floor(minY), math.floor(maxY)
    fnames = set([
            aster30m_fname(minX,minY),
            aster30m_fname(minX,maxY),
            aster30m_fname(maxX,minY),
            aster30m_fname(maxX,maxY),
    ])
    srcs = [rasterio.open(fname) for fname in fnames]
    if len(srcs) == 1:
        return srcs[0]
    
    # damn we have to stitch multiple rasters together
    data,transform = merge(srcs)
    meta = srcs[0].meta.copy()
    meta.update({
            'height': data.shape[1],
            'width': data.shape[2],
            'transform': transform,
    })
    with rasterio.open(temp_fname, 'w', **meta) as f:
        f.write(data)
    return rasterio.open(temp_fname)
    
def load_elevation_and_mask(polygon):
    polygon_json = [mapping(polygon)] # convert to GeoJSON format
    bbox_json = [mapping(box(*polygon.bounds))]
    # get bounding box as polygon, then use `mask` to crop raster to 
    # the bounding box
    # there's probably a much neater way!
    raster = load_raster_file(polygon)
    values,_ = mask(raster, polygon_json, crop=True, nodata=-9999)
    area_mask = values[0] != -9999 # True inside mining area
    values,_ = mask(raster, bbox_json, crop=True)
    values = values[0] # elevation for whole bounding box
    assert(values.shape == area_mask.shape)
    return values, area_mask
    
def do_flooding(AA, FF, RR, flood_elev):
    # raise the flood water, find all new cells that get wet
    # AA: elevation profile
    # FF: True for cells that have been flooded (return new one)
    # RR: minimum flood water elevation that got a particular cell wet (write to by reference)
    # flood_elev: elevation to raise flood water to
    while True:
        FF_prime = (
            np.roll(np.roll(FF,  1, 0),  1, 1) + 
            np.roll(np.roll(FF,  1, 0), -1, 1) + 
            np.roll(np.roll(FF, -1, 0),  1, 1) + 
            np.roll(np.roll(FF, -1, 0), -1, 1) + 
            np.roll(FF,  1, 0) + 
            np.roll(FF, -1, 0) + 
            np.roll(FF,  1, 1) + 
            np.roll(FF, -1, 1) + 
            FF
        ) * (AA <= flood_elev)
        if np.all(FF_prime == FF):
            return FF
        RR[np.where(FF_prime * ~FF)] = flood_elev
        FF[:,:] = FF_prime[:,:]
        
# make each pit point a square, and combine into polygon
def create_pit_poly(lat, lon, DD):
    pts = np.float32(np.transpose(np.where(DD>0)))
    pts[:,0] = lat - pts[:,0]/3600.0
    pts[:,1] = lon + pts[:,1]/3600.0
    radius = 0.55 * res # pixel resolution in deg
    squares = [Point(lon,lat).buffer(radius).envelope for lat,lon in pts]
    shape = cascaded_union(squares)
    #assert(shape.type == 'Polygon') # shouldn't be mutlipolygon
    return shape

def mine_area_fname(poly):
    b = poly.bounds
    # format: min_lat,min_lon,max_lat,max_lon
    return f'{b[1]:.5f},{b[0]:.5f},{b[3]:.5f},{b[2]:.5f}'
    
############
### main ###
############
    
shapefile = gpd.read_file(input_fname)
#shapefile = shapefile.sort_values('qgis-area')[::-1]

indices = list(range(len(shapefile)))
random.shuffle(indices)
errors = []

for i in progressbar.progressbar(indices):
    
    try:
    
        row = shapefile.iloc[i]
        poly = row.geometry
        
        fname = os.path.join(output_dir, mine_area_fname(poly) + '.geojson')
        if os.path.exists(fname):
            continue
        
        lat, lon = poly.bounds[3], poly.bounds[0] # coords of 0,0 pixel (top left on map)
        
        elevation, area_mask = load_elevation_and_mask(poly)
        s0,s1 = elevation.shape
        
        m_per_lat, m_per_long = latlon_scaling(lat,lon)
        area_per_square_unit = m_per_lat/3600 * m_per_long/3600
        volume_per_cubic_unit = 1.0 * area_per_square_unit / 1e6 # GL
        
        # AA, MM, FF, RR, CC and DD are all 2d numpy array of the same shape...
        
        # MM == True inside the mining area
        MM = np.zeros((s0+2, s1+2), dtype='bool')
        MM[1:-1,1:-1] = area_mask
        if np.sum(MM) == 0:
            with open(fname, 'w') as f:
                f.write('')
            continue
        
        # AA is elevation profile, with a 1 pixel wide boarder
        AA = np.zeros(MM.shape, dtype=elevation.dtype)
        AA[1:-1,1:-1] = elevation
        min_elev = AA[MM==True].min()
        max_elev = AA[MM==True].max()
        AA[MM==False] = min_elev
        
        ## FF == True where currently flooded, start outside mining area
        FF = ~(MM.copy())
            
        ## RR is result, the flood elevation at which that pixel gets wet
        RR = np.zeros(AA.shape)
        RR[MM==False] = min_elev
            
        for elev in range(min_elev+1, max_elev+1):
            FF = do_flooding(AA, FF, RR, elev)
            
        ## DD for depth (between flooded water and elevation)
        DD = RR - AA - top_edge_margin 
        DD[DD < 0] = 0
        
        connecting_structure = np.ones((3,3))
        CC, num_pits = label(DD, structure=connecting_structure) ## CC for connected components
        
        pits = []
        for i in range(1, num_pits+1):
            DD_pit = (CC==i)*DD # DD for pit depth profile
            
            area = np.sum(CC==i) * area_per_square_unit
            if area < minimum_area:
                continue
            
            volume = np.sum(DD_pit) * volume_per_cubic_unit
            if volume < minimum_volume:
                continue
            
            depth = DD_pit.max()
            if depth < minimum_depth:
                continue
            
            elev_top = RR[CC==i][0] - top_edge_margin
            elev_bottom = elev_top - depth
            pit = {
                'pit_depth': depth,
                'geometry': create_pit_poly(lat, lon, DD_pit),
            }
            
            # volume as a function of water level elevation:
            for i in range(100, 0, -10):
                elev = elev_bottom + i/100*pit['pit_depth']
                volume = np.sum(np.maximum(0, DD_pit - (elev_top - elev)))
                pit['elev_' + str(i)] = elev
                pit['volume_' + str(i)] = volume * volume_per_cubic_unit
            
            pits.append(pit)
        
        if len(pits) > 0:
            pits = gpd.GeoDataFrame(pits)
            pits.to_file(fname, driver='GeoJSON')
        else:
            with open(fname, 'w') as f:
                f.write('')
                
    except Exception as e:
        errors.append(fname.split('/')[-1])
        #print(f'[!] ERROR for file {fname}: {e}')

print('[!] Files not processed:')
print('\n'.join(errors))