
# Mining Area Data Sources

## 1. Open Street Maps

All areas with a "landuse=quarry" tag.

Downloaded with `build_database.py`

101550 polygons
40838 km2

Note, could filter by excluding quarries that have the tag "underground=yes"

## 2. Global Mining

Paper titled "A global-scale data set of mining areas" 
(`https://www.nature.com/articles/s41597-020-00624-w`).

Dataset of mining global mining areas identified from satellite imagery.

21060 polygons
57291.7 km2

## Combined

115003 polygons
83179.6 km2

Biggest: 1354.8 km2, west coast of South America




# Method

1. Download OSM mining areas (`build_database.py`) and "Global Mining" database. Merge areas from both datasets into one in QGIS:
- "Merge Vector Layers", don't use "Union" as it takes too long (and just crashes)
- Then use "Dissolve" to combine overlapping polygons
- Then use "Multipart to Singleparts" to split the one multipolygon into one polygon for each mining area.

2. Run pit finding script on the mining areas (`find_pits.py`). Takes about 8 hours. For each mining area, the pits found are saved to a one .geojson file (or an empty file) in a cache directory. So if the script crashes, fails or is stopped, next time it runs it can skip mining areas that have been done previously.

3. Combine cached results (`insert_into_database.py`) into one file/table. By far the quickest and easiest way to do this is to add all the .geojson files into a PostGIS database, since the resulting file/table can be several gigabytes without any problems. If you attempt to combine the .geojson files in memory using a python script, you'll get out of memory errors very quickly.

4. Post processing in QGIS. Firstly the pits should have their area calculated. Then the two buffer steps makes the shapes look nices (not just sqaures with corners slightly touching). The next step removes holes, needed for Ryan's reservoir matching code. 

- "Field Calculator" tool, add column called "pit_area" using formula "$area".
- "Buffer" tool, with distance set to 0.4*res (where res is the GDEM resolution in degrees = 1/3600), and join style is bevel.
- "Buffer" again with distance set to -0.4*res and join style is bevel.
- "Delete holes" tool, with minimum hole area set really large.
- "Field Calculator" tool, add column called "lat" with formula "y($geometry)"
- "Field Calculator" tool, add column called "lon" with formula "x($geometry)"
- "Field Calculator" tool, add column called "name" with formular "uuid()"
- "Refactor field" tool, to rearrange column order, make "name" column a string and "pit_depth" column an integer.




# Ryan Format 

- A shapefile with all of the reservoirs to be considered.
- A csv file that extracts all of the names of the reservoirs in the order they appear in the shapefile
- A csv file that has the Name,lat,long,elevation (in metres),volume (in GL) for the reservoirs to be modelled.




