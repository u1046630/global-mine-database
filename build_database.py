#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 15:24:20 2020

@author: liam

download all quarries from OSM, takes about 15 minutes and 72 requests.
"""
from src import OsmDownloader

search_terms = [ 
    'landuse=quarry',
]
tile_size = 10
downloader = OsmDownloader(
        search_terms, 
        tile_size,
        'data/osm_cache/overpass-downloaded',
        'data/osm_cache/geojson-converted',
        'data/osm_cache/geojson-filtered'
)
downloader.download()
downloader.convert()
downloader.show_missing()

gdf = downloader.combine()
columns = [
    'landuse',
    'id',
    'calced_area',
    'geometry',
    'source',
    'resource',
    'name',
    'type',
    'operator',
]

gdf = gdf.drop(columns=set(gdf.columns)-set(columns))
gdf.to_file('global_osm_mines.shp')