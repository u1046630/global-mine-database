#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 11:00:26 2020

@author: liam

TODO: make the whole pipeline runable from this class
Download > Convert-to-Geojson > Convert-to-shapefile > Filter-by-area
"""

import os, random, time, progressbar
import threading, queue
import json, requests
import pandas as pd, geopandas as gpd, pyproj
import warnings

# Overpass API servers, can round robin to reduce server load
urls = [
    'https://overpass-api.de/api/interpreter',
    'http://overpass.openstreetmap.fr/api/interpreter',
    'https://overpass.kumi.systems/api/interpreter',
    #'https://overpass.nchc.org.tw/api/interpreter',
]

base_query = '''
[out:json];
(
{snips}
);
out body;
>;
out skel qt;
'''   

# small pieces of Overpass queries that can be combined together
query_snips = {
    'water=reservoir': 
        'way["natural"="water"]["water"="reservoir"]({bbox});\n' + 
        'relation["natural"="water"]["water"="reservoir"]({bbox});',
    'landuse=reservoir':
        'way["landuse"="reservoir"]({bbox});\n' + 
        'relation["landuse"="reservoir"]({bbox});',
    'water=lake':
        'way["water"="lake"]({bbox});\n' + 
        'relation["water"="lake"]({bbox});',
    'landuse=quarry':
        'way["landuse"="quarry"]({bbox});\n' + 
        'relation["landuse"="quarry"]({bbox});',
}
    
    
geod = pyproj.Geod(ellps='WGS84')

# distance in meters between two points in WGS84
def dist(lat1, lon1, lat2, lon2):
    return geod.inv(lon1, lat1, lon2, lat2)[2]

# area in m2 of polygon defined on WGS84 coordinates
def calc_size(poly):
    c = poly.centroid
    lat,lon = c.y, c.x
    # conversion factor: square m per square lat/lon degree
    multiplier = dist(lat-0.05, lon, lat+0.05, lon) * dist(lat, lon-0.05, lat, lon+0.05) * 10 * 10
    return poly.area * multiplier


class OsmDownloader(object):
    
    def __init__(self, search_terms, tile_size, cache_json, cache_geojson1, cache_geojson2):
        self.cache_json = cache_json
        self.cache_geojson1 = cache_geojson1
        self.cache_geojson2 = cache_geojson2
        assert(os.path.isdir(cache_json))
        assert(os.path.isdir(cache_geojson1))
        assert(os.path.isdir(cache_geojson2))
        
        snip_list = []
        for term in search_terms:
            if term in query_snips:
                snip_list.append(query_snips[term])
            else:
                raise Exception(f'[!] I don\'t have a query snip for the search term "{term}"')
        self.query = base_query.format(snips='\n'.join(snip_list))
        
        if 180 % tile_size != 0:
            raise Exception('[!] tile_size must divide 180')
        self.tile_size = tile_size
        self.tiles = []
        for lon in range(-180, 180, tile_size):
            for lat in range(-90, 90, tile_size):
                tile = '{},{},{},{}'.format(lat, lon, lat+tile_size, lon+tile_size)
                self.tiles.append(tile)
        random.shuffle(self.tiles)
        
    def show_missing(self):
        missing_json = []
        missing_geojson1 = []
        missing_geojson2 = []
        for tile in self.tiles:
            if not os.path.exists(os.path.join(self.cache_json, tile + '.json')):
                missing_json.append(tile)
            if not os.path.exists(os.path.join(self.cache_geojson1, tile + '.geojson')):
                missing_geojson1.append(tile)
            if not os.path.exists(os.path.join(self.cache_geojson2, tile + '.geojson')):
                missing_geojson2.append(tile)
        print('[*] missing downloaded JSON: \n' + '\n'.join(missing_json))
        print('[*] missing converted GEOJSON: \n' + '\n'.join(missing_geojson1))
        print('[*] missing filtered GEOJSON: \n' + '\n'.join(missing_geojson2))
        
        
    def convert(self, num_threads=16, min_area=10000):
        self.min_area = min_area # minimum polygon area in m2 to filter out
        
        jobs = queue.Queue()
        signals = queue.Queue()
        
        threads = []
        for i in range(num_threads):
            args = (self, jobs, signals)
            thread = threading.Thread(target=OsmDownloader.convert_worker, args=args)
            thread.start()
            threads.append(thread)
            
        for tile in progressbar.progressbar(self.tiles):
            #if not tile.startswith('45,115'):
            #    continue
            signals.get()
            jobs.put(tile)
            
        for thread in threads:
            jobs.put(None)
            
        for thread in threads:
            thread.join()
            
        print('[*] conversion complete!', flush=True)
            
    def convert_worker(self, jobs, signals):
        signals.put('ready')
        while True:
            tile = jobs.get()
            if tile is None:
                break
            self.convert_func(tile)
            signals.put('ready')  
            
    def convert_func(self, tile):
        fname_json = os.path.join(self.cache_json, tile + '.json')
        fname_geojson1 = os.path.join(self.cache_geojson1, tile + '.geojson')
        fname_geojson2 = os.path.join(self.cache_geojson2, tile + '.geojson')
        
        # convert JSON to GEOJSON
        if os.path.exists(fname_json) and not os.path.exists(fname_geojson1):
            # check if JSON is valid (delete if not)
            if os.path.exists(fname_json):
                try:
                    with open(fname_json, 'r') as f:
                        json.loads(f.read())
                except ValueError:
                    os.remove(fname_json)
                    
            if os.path.exists(fname_json):
                os.system(f'osmtogeojson {fname_json} > {fname_geojson1}')
        
        if os.path.exists(fname_geojson1) and not os.path.exists(fname_geojson2):
            
            # 0 byte converted GEOJSON means it wasn't finished converting:
            if os.stat(fname_geojson1).st_size == 0:
                os.remove(fname_geojson1) 
                return
            
            try:
                gdf = gpd.read_file(fname_geojson1)
            except:
                print(f'\n[!] read error for tile {tile}', flush=True)
                return
            
            # only keep features whose centroid is inside the tile bounds
            # shapely is calculating centroid in flat geometry not geoid
            # (so throws Warning) but it's good enough to deduplicate
            warnings.simplefilter("ignore", UserWarning)
            bounds = [float(i) for i in tile.split(',')]
            centroids = gdf.centroid
            gdf = gdf[
                (centroids.y >= bounds[0]) & 
                (centroids.y <  bounds[2]) & 
                (centroids.x >= bounds[1]) & 
                (centroids.x <  bounds[3])
            ]
            
            # filter out small features
            areas = []
            for poly in gdf.geometry:
                areas.append(calc_size(poly))
            gdf['calced_area'] = areas
            gdf = gdf[gdf.calced_area > self.min_area]
            if len(gdf) == 0:
                # can't write empty dataframe to file, so write empty file
                with open(fname_geojson2, 'w') as f:
                    f.write('')
            else:
                gdf.to_file(fname_geojson2, driver='GeoJSON')
                

            
    def download(self):
        
        jobs = queue.Queue() # give jobs to workers (lat,lon) tuple
        signals = queue.Queue() # workers signal job is done
        
        threads = []
        for url in urls:
            args = (self, url, jobs, signals)
            thread = threading.Thread(target=OsmDownloader.download_worker, args=args)
            thread.start()
            threads.append(thread)
            
        for tile in progressbar.progressbar(self.tiles):
            signals.get()
            jobs.put(tile)
            
        for thread in threads:
            jobs.put(None)
        
        for thread in threads:
            thread.join()
            
        print('[*] download complete!', flush=True)
        
    def download_worker(self, url, jobs, signals):
        signals.put('ready')
        while True:
            tile = jobs.get()
            if tile is None:
                break
            self.download_func(url, tile)
            signals.put('ready')
        
    def download_func(self, url, tile):
        fname = os.path.join(self.cache_json, f'{tile}.json')
        if os.path.exists(fname):
            return
        query = self.query.format(bbox=tile)
        response = requests.get(url, params={'data': query})
        if response.status_code == 429: # Too many requests:
            print(f'\n[*] Too many requests ({url})')
            time.sleep(10)
            return
        elif response.status_code == 504:
            print(f'\n[*] Gateway timeout ({url})')
            return
        elif response.status_code != 200:
            print(f'\n[!] Status code {response.status_code} ({url})')
        with open(fname, 'wb') as f:
            f.write(response.content)

    def combine(self):
        df = pd.DataFrame()
        for tile in progressbar.progressbar(self.tiles):
            fname = os.path.join(self.cache_geojson2, tile + '.geojson')
            if not os.path.exists(fname):
                print(f'[!] file doesnt exist: {tile} (skipping)')
                continue
            if os.stat(fname).st_size == 0:
                continue
            try:
                gdf = gpd.read_file(fname)
            except:
                print(f'[!] failed to read {tile}')
                continue   
            df = pd.concat([df, gdf])
        return df
    